#include "cpptest.h"

/* CPPTEST_TEST_SUITE_CODE_BEGIN AdditionalIncludes */
/* CPPTEST_TEST_SUITE_CODE_END AdditionalIncludes */

CPPTEST_CONTEXT("/FlowAnalysisCpp/DeadLock.cpp");
CPPTEST_TEST_SUITE_INCLUDED_TO("/FlowAnalysisCpp/DeadLock.cpp");

class TestSuite_DeadLock_cpp_2788e23e : public CppTest_TestSuite
{
    public:
        CPPTEST_TEST_SUITE(TestSuite_DeadLock_cpp_2788e23e);
        CPPTEST_TEST_SUITE_SETUP(testSuiteSetUp);
        CPPTEST_TEST(test_Controller_Thread_1);
        CPPTEST_TEST(test_GameLogic_Thread_1);
        CPPTEST_TEST(test_Physics_Thread_1);
        CPPTEST_TEST(test_Physics_Thread_10);
        CPPTEST_TEST(test_Physics_Thread_2);
        CPPTEST_TEST(test_Physics_Thread_3);
        CPPTEST_TEST(test_Physics_Thread_4);
        CPPTEST_TEST(test_Physics_Thread_5);
        CPPTEST_TEST(test_Physics_Thread_6);
        CPPTEST_TEST(test_Physics_Thread_7);
        CPPTEST_TEST(test_Physics_Thread_8);
        CPPTEST_TEST(test_Physics_Thread_9);
        CPPTEST_TEST(test_Render_Thread_1);
        CPPTEST_TEST(test_Render_Thread_10);
        CPPTEST_TEST(test_Render_Thread_2);
        CPPTEST_TEST(test_Render_Thread_3);
        CPPTEST_TEST(test_Render_Thread_4);
        CPPTEST_TEST(test_Render_Thread_5);
        CPPTEST_TEST(test_Render_Thread_6);
        CPPTEST_TEST(test_Render_Thread_7);
        CPPTEST_TEST(test_Render_Thread_8);
        CPPTEST_TEST(test_Render_Thread_9);
        CPPTEST_TEST(test_assertion_1);
        CPPTEST_TEST(test_assertion_10);
        CPPTEST_TEST(test_assertion_2);
        CPPTEST_TEST(test_assertion_3);
        CPPTEST_TEST(test_assertion_4);
        CPPTEST_TEST(test_assertion_5);
        CPPTEST_TEST(test_assertion_6);
        CPPTEST_TEST(test_assertion_7);
        CPPTEST_TEST(test_assertion_8);
        CPPTEST_TEST(test_assertion_9);
        CPPTEST_TEST(test_draw_1);
        CPPTEST_TEST(test_runGameThreads2_1);
        CPPTEST_TEST(test_runGameThreads2_10);
        CPPTEST_TEST(test_runGameThreads2_2);
        CPPTEST_TEST(test_runGameThreads2_3);
        CPPTEST_TEST(test_runGameThreads2_4);
        CPPTEST_TEST(test_runGameThreads2_5);
        CPPTEST_TEST(test_runGameThreads2_6);
        CPPTEST_TEST(test_runGameThreads2_7);
        CPPTEST_TEST(test_runGameThreads2_8);
        CPPTEST_TEST(test_runGameThreads2_9);
        CPPTEST_TEST(test_runGameThreads_1);
        CPPTEST_TEST(test_runGameThreads_10);
        CPPTEST_TEST(test_runGameThreads_2);
        CPPTEST_TEST(test_runGameThreads_3);
        CPPTEST_TEST(test_runGameThreads_4);
        CPPTEST_TEST(test_runGameThreads_5);
        CPPTEST_TEST(test_runGameThreads_6);
        CPPTEST_TEST(test_runGameThreads_7);
        CPPTEST_TEST(test_runGameThreads_8);
        CPPTEST_TEST(test_runGameThreads_9);
        CPPTEST_TEST_SUITE_TEARDOWN(testSuiteTearDown);
        CPPTEST_TEST_SUITE_END();
        
        static void testSuiteSetUp();
        static void testSuiteTearDown();

        void setUp();
        void tearDown();

        void test_Controller_Thread_1();
        void test_GameLogic_Thread_1();
        void test_Physics_Thread_1();
        void test_Physics_Thread_10();
        void test_Physics_Thread_2();
        void test_Physics_Thread_3();
        void test_Physics_Thread_4();
        void test_Physics_Thread_5();
        void test_Physics_Thread_6();
        void test_Physics_Thread_7();
        void test_Physics_Thread_8();
        void test_Physics_Thread_9();
        void test_Render_Thread_1();
        void test_Render_Thread_10();
        void test_Render_Thread_2();
        void test_Render_Thread_3();
        void test_Render_Thread_4();
        void test_Render_Thread_5();
        void test_Render_Thread_6();
        void test_Render_Thread_7();
        void test_Render_Thread_8();
        void test_Render_Thread_9();
        void test_assertion_1();
        void test_assertion_10();
        void test_assertion_2();
        void test_assertion_3();
        void test_assertion_4();
        void test_assertion_5();
        void test_assertion_6();
        void test_assertion_7();
        void test_assertion_8();
        void test_assertion_9();
        void test_draw_1();
        void test_runGameThreads2_1();
        void test_runGameThreads2_10();
        void test_runGameThreads2_2();
        void test_runGameThreads2_3();
        void test_runGameThreads2_4();
        void test_runGameThreads2_5();
        void test_runGameThreads2_6();
        void test_runGameThreads2_7();
        void test_runGameThreads2_8();
        void test_runGameThreads2_9();
        void test_runGameThreads_1();
        void test_runGameThreads_10();
        void test_runGameThreads_2();
        void test_runGameThreads_3();
        void test_runGameThreads_4();
        void test_runGameThreads_5();
        void test_runGameThreads_6();
        void test_runGameThreads_7();
        void test_runGameThreads_8();
        void test_runGameThreads_9();
};

CPPTEST_TEST_SUITE_REGISTRATION(TestSuite_DeadLock_cpp_2788e23e);

void TestSuite_DeadLock_cpp_2788e23e::testSuiteSetUp()
{
/* CPPTEST_TEST_SUITE_CODE_BEGIN TestSuiteSetUp */
/* CPPTEST_TEST_SUITE_CODE_END TestSuiteSetUp */
}

void TestSuite_DeadLock_cpp_2788e23e::testSuiteTearDown()
{
/* CPPTEST_TEST_SUITE_CODE_BEGIN TestSuiteTearDown */
/* CPPTEST_TEST_SUITE_CODE_END TestSuiteTearDown */
}

void TestSuite_DeadLock_cpp_2788e23e::setUp()
{
/* CPPTEST_TEST_SUITE_CODE_BEGIN TestCaseSetUp */
/* CPPTEST_TEST_SUITE_CODE_END TestCaseSetUp */
}

void TestSuite_DeadLock_cpp_2788e23e::tearDown()
{
/* CPPTEST_TEST_SUITE_CODE_BEGIN TestCaseTearDown */
/* CPPTEST_TEST_SUITE_CODE_END TestCaseTearDown */
}

/* CPPTEST_TEST_CASE_BEGIN test_Controller_Thread_1 */
/* CPPTEST_TEST_CASE_CONTEXT DWORD Controller_Thread(void *) */
void TestSuite_DeadLock_cpp_2788e23e::test_Controller_Thread_1()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (unknown) */ 
    void * _arg1  = 0 ;
    /* Tested function call */
    ::DWORD _return  = ::Controller_Thread(_arg1);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_UINTEGER("::DWORD _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("void * _arg1 ", ( _arg1 ));
}
/* CPPTEST_TEST_CASE_END test_Controller_Thread_1 */

/* CPPTEST_TEST_CASE_BEGIN test_GameLogic_Thread_1 */
/* CPPTEST_TEST_CASE_CONTEXT DWORD GameLogic_Thread(void *) */
void TestSuite_DeadLock_cpp_2788e23e::test_GameLogic_Thread_1()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (unknown) */ 
    void * _arg1  = 0 ;
    /* Tested function call */
    ::DWORD _return  = ::GameLogic_Thread(_arg1);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_UINTEGER("::DWORD _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("void * _arg1 ", ( _arg1 ));
}
/* CPPTEST_TEST_CASE_END test_GameLogic_Thread_1 */

/* CPPTEST_TEST_CASE_BEGIN test_Physics_Thread_1 */
/* CPPTEST_TEST_CASE_CONTEXT DWORD Physics::Physics_Thread(void *) */
void TestSuite_DeadLock_cpp_2788e23e::test_Physics_Thread_1()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (unknown) */ 
    void * _arg1  = 0 ;
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = 100;
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 0;
         ::changePositionMutex.RecursionCount  = 0;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 0;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Tested function call */
    ::DWORD _return  = ::Physics::Physics_Thread(_arg1);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_UINTEGER("::DWORD _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("void * _arg1 ", ( _arg1 ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
}
/* CPPTEST_TEST_CASE_END test_Physics_Thread_1 */

/* CPPTEST_TEST_CASE_BEGIN test_Physics_Thread_10 */
/* CPPTEST_TEST_CASE_CONTEXT DWORD Physics::Physics_Thread(void *) */
void TestSuite_DeadLock_cpp_2788e23e::test_Physics_Thread_10()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (unknown) */ 
    void * _arg1  = 0 ;
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = 100;
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = cpptestLimitsGetMinLong();
         ::changePositionMutex.RecursionCount  = -1L;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 0ul;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Tested function call */
    ::DWORD _return  = ::Physics::Physics_Thread(_arg1);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_UINTEGER("::DWORD _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("void * _arg1 ", ( _arg1 ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
}
/* CPPTEST_TEST_CASE_END test_Physics_Thread_10 */

/* CPPTEST_TEST_CASE_BEGIN test_Physics_Thread_2 */
/* CPPTEST_TEST_CASE_CONTEXT DWORD Physics::Physics_Thread(void *) */
void TestSuite_DeadLock_cpp_2788e23e::test_Physics_Thread_2()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (unknown) */ 
    void * _arg1  = 0 ;
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = cpptestLimitsGetMaxInt();
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = 100;
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 0;
         ::changePositionMutex.RecursionCount  = 0;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 0;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Tested function call */
    ::DWORD _return  = ::Physics::Physics_Thread(_arg1);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_UINTEGER("::DWORD _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("void * _arg1 ", ( _arg1 ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
}
/* CPPTEST_TEST_CASE_END test_Physics_Thread_2 */

/* CPPTEST_TEST_CASE_BEGIN test_Physics_Thread_3 */
/* CPPTEST_TEST_CASE_CONTEXT DWORD Physics::Physics_Thread(void *) */
void TestSuite_DeadLock_cpp_2788e23e::test_Physics_Thread_3()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (unknown) */ 
    void * _arg1  = 0 ;
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = 100;
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 1L;
         ::changePositionMutex.RecursionCount  = cpptestLimitsGetMinLong();
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 0;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Tested function call */
    ::DWORD _return  = ::Physics::Physics_Thread(_arg1);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_UINTEGER("::DWORD _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("void * _arg1 ", ( _arg1 ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
}
/* CPPTEST_TEST_CASE_END test_Physics_Thread_3 */

/* CPPTEST_TEST_CASE_BEGIN test_Physics_Thread_4 */
/* CPPTEST_TEST_CASE_CONTEXT DWORD Physics::Physics_Thread(void *) */
void TestSuite_DeadLock_cpp_2788e23e::test_Physics_Thread_4()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (unknown) */ 
    void * _arg1  = 0 ;
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = 100;
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = cpptestLimitsGetMinLong();
         ::changePositionMutex.RecursionCount  = 0L;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 0;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Tested function call */
    ::DWORD _return  = ::Physics::Physics_Thread(_arg1);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_UINTEGER("::DWORD _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("void * _arg1 ", ( _arg1 ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
}
/* CPPTEST_TEST_CASE_END test_Physics_Thread_4 */

/* CPPTEST_TEST_CASE_BEGIN test_Physics_Thread_5 */
/* CPPTEST_TEST_CASE_CONTEXT DWORD Physics::Physics_Thread(void *) */
void TestSuite_DeadLock_cpp_2788e23e::test_Physics_Thread_5()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (unknown) */ 
    void * _arg1  = 0 ;
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = 100;
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 0;
         ::changePositionMutex.RecursionCount  = cpptestLimitsGetMinLong();
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = cpptestLimitsGetMaxUnsignedLong();
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Tested function call */
    ::DWORD _return  = ::Physics::Physics_Thread(_arg1);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_UINTEGER("::DWORD _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("void * _arg1 ", ( _arg1 ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
}
/* CPPTEST_TEST_CASE_END test_Physics_Thread_5 */

/* CPPTEST_TEST_CASE_BEGIN test_Physics_Thread_6 */
/* CPPTEST_TEST_CASE_CONTEXT DWORD Physics::Physics_Thread(void *) */
void TestSuite_DeadLock_cpp_2788e23e::test_Physics_Thread_6()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (unknown) */ 
    void * _arg1  = 0 ;
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = 100;
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 1L;
         ::changePositionMutex.RecursionCount  = 1L;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = cpptestLimitsGetMaxUnsignedLong();
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Tested function call */
    ::DWORD _return  = ::Physics::Physics_Thread(_arg1);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_UINTEGER("::DWORD _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("void * _arg1 ", ( _arg1 ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
}
/* CPPTEST_TEST_CASE_END test_Physics_Thread_6 */

/* CPPTEST_TEST_CASE_BEGIN test_Physics_Thread_7 */
/* CPPTEST_TEST_CASE_CONTEXT DWORD Physics::Physics_Thread(void *) */
void TestSuite_DeadLock_cpp_2788e23e::test_Physics_Thread_7()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (unknown) */ 
    void * _arg1  = 0 ;
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = 100;
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = cpptestLimitsGetMinLong();
         ::changePositionMutex.RecursionCount  = cpptestLimitsGetMaxLong();
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 1ul;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Tested function call */
    ::DWORD _return  = ::Physics::Physics_Thread(_arg1);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_UINTEGER("::DWORD _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("void * _arg1 ", ( _arg1 ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
}
/* CPPTEST_TEST_CASE_END test_Physics_Thread_7 */

/* CPPTEST_TEST_CASE_BEGIN test_Physics_Thread_8 */
/* CPPTEST_TEST_CASE_CONTEXT DWORD Physics::Physics_Thread(void *) */
void TestSuite_DeadLock_cpp_2788e23e::test_Physics_Thread_8()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (unknown) */ 
    void * _arg1  = 0 ;
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = 100;
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 0;
         ::changePositionMutex.RecursionCount  = 1L;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 1ul;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Tested function call */
    ::DWORD _return  = ::Physics::Physics_Thread(_arg1);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_UINTEGER("::DWORD _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("void * _arg1 ", ( _arg1 ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
}
/* CPPTEST_TEST_CASE_END test_Physics_Thread_8 */

/* CPPTEST_TEST_CASE_BEGIN test_Physics_Thread_9 */
/* CPPTEST_TEST_CASE_CONTEXT DWORD Physics::Physics_Thread(void *) */
void TestSuite_DeadLock_cpp_2788e23e::test_Physics_Thread_9()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (unknown) */ 
    void * _arg1  = 0 ;
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = 100;
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 1L;
         ::changePositionMutex.RecursionCount  = 0;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 0ul;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Tested function call */
    ::DWORD _return  = ::Physics::Physics_Thread(_arg1);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_UINTEGER("::DWORD _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("void * _arg1 ", ( _arg1 ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
}
/* CPPTEST_TEST_CASE_END test_Physics_Thread_9 */

/* CPPTEST_TEST_CASE_BEGIN test_Render_Thread_1 */
/* CPPTEST_TEST_CASE_CONTEXT DWORD Render::Render_Thread(void *) */
void TestSuite_DeadLock_cpp_2788e23e::test_Render_Thread_1()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (unknown) */ 
    void * _arg1  = 0 ;
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = 100;
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 0;
         ::changePositionMutex.RecursionCount  = 0;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 0;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::DWORD _return  = ::Render::Render_Thread(_arg1);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_UINTEGER("::DWORD _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("void * _arg1 ", ( _arg1 ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_Render_Thread_1 */

/* CPPTEST_TEST_CASE_BEGIN test_Render_Thread_10 */
/* CPPTEST_TEST_CASE_CONTEXT DWORD Render::Render_Thread(void *) */
void TestSuite_DeadLock_cpp_2788e23e::test_Render_Thread_10()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (unknown) */ 
    void * _arg1  = 0 ;
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = 100;
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = cpptestLimitsGetMinLong();
         ::changePositionMutex.RecursionCount  = -1L;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 0ul;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::DWORD _return  = ::Render::Render_Thread(_arg1);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_UINTEGER("::DWORD _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("void * _arg1 ", ( _arg1 ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_Render_Thread_10 */

/* CPPTEST_TEST_CASE_BEGIN test_Render_Thread_2 */
/* CPPTEST_TEST_CASE_CONTEXT DWORD Render::Render_Thread(void *) */
void TestSuite_DeadLock_cpp_2788e23e::test_Render_Thread_2()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (unknown) */ 
    void * _arg1  = 0 ;
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = cpptestLimitsGetMaxInt();
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = 100;
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 0;
         ::changePositionMutex.RecursionCount  = 0;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 0;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::DWORD _return  = ::Render::Render_Thread(_arg1);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_UINTEGER("::DWORD _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("void * _arg1 ", ( _arg1 ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_Render_Thread_2 */

/* CPPTEST_TEST_CASE_BEGIN test_Render_Thread_3 */
/* CPPTEST_TEST_CASE_CONTEXT DWORD Render::Render_Thread(void *) */
void TestSuite_DeadLock_cpp_2788e23e::test_Render_Thread_3()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (unknown) */ 
    void * _arg1  = 0 ;
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = 100;
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 1L;
         ::changePositionMutex.RecursionCount  = cpptestLimitsGetMinLong();
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 0;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::DWORD _return  = ::Render::Render_Thread(_arg1);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_UINTEGER("::DWORD _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("void * _arg1 ", ( _arg1 ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_Render_Thread_3 */

/* CPPTEST_TEST_CASE_BEGIN test_Render_Thread_4 */
/* CPPTEST_TEST_CASE_CONTEXT DWORD Render::Render_Thread(void *) */
void TestSuite_DeadLock_cpp_2788e23e::test_Render_Thread_4()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (unknown) */ 
    void * _arg1  = 0 ;
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = 100;
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = cpptestLimitsGetMinLong();
         ::changePositionMutex.RecursionCount  = 0L;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 0;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::DWORD _return  = ::Render::Render_Thread(_arg1);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_UINTEGER("::DWORD _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("void * _arg1 ", ( _arg1 ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_Render_Thread_4 */

/* CPPTEST_TEST_CASE_BEGIN test_Render_Thread_5 */
/* CPPTEST_TEST_CASE_CONTEXT DWORD Render::Render_Thread(void *) */
void TestSuite_DeadLock_cpp_2788e23e::test_Render_Thread_5()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (unknown) */ 
    void * _arg1  = 0 ;
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = 100;
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 0;
         ::changePositionMutex.RecursionCount  = cpptestLimitsGetMinLong();
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = cpptestLimitsGetMaxUnsignedLong();
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::DWORD _return  = ::Render::Render_Thread(_arg1);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_UINTEGER("::DWORD _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("void * _arg1 ", ( _arg1 ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_Render_Thread_5 */

/* CPPTEST_TEST_CASE_BEGIN test_Render_Thread_6 */
/* CPPTEST_TEST_CASE_CONTEXT DWORD Render::Render_Thread(void *) */
void TestSuite_DeadLock_cpp_2788e23e::test_Render_Thread_6()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (unknown) */ 
    void * _arg1  = 0 ;
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = 100;
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 1L;
         ::changePositionMutex.RecursionCount  = 1L;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = cpptestLimitsGetMaxUnsignedLong();
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::DWORD _return  = ::Render::Render_Thread(_arg1);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_UINTEGER("::DWORD _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("void * _arg1 ", ( _arg1 ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_Render_Thread_6 */

/* CPPTEST_TEST_CASE_BEGIN test_Render_Thread_7 */
/* CPPTEST_TEST_CASE_CONTEXT DWORD Render::Render_Thread(void *) */
void TestSuite_DeadLock_cpp_2788e23e::test_Render_Thread_7()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (unknown) */ 
    void * _arg1  = 0 ;
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = 100;
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = cpptestLimitsGetMinLong();
         ::changePositionMutex.RecursionCount  = cpptestLimitsGetMaxLong();
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 1ul;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::DWORD _return  = ::Render::Render_Thread(_arg1);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_UINTEGER("::DWORD _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("void * _arg1 ", ( _arg1 ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_Render_Thread_7 */

/* CPPTEST_TEST_CASE_BEGIN test_Render_Thread_8 */
/* CPPTEST_TEST_CASE_CONTEXT DWORD Render::Render_Thread(void *) */
void TestSuite_DeadLock_cpp_2788e23e::test_Render_Thread_8()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (unknown) */ 
    void * _arg1  = 0 ;
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = 100;
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 0;
         ::changePositionMutex.RecursionCount  = 1L;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 1ul;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::DWORD _return  = ::Render::Render_Thread(_arg1);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_UINTEGER("::DWORD _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("void * _arg1 ", ( _arg1 ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_Render_Thread_8 */

/* CPPTEST_TEST_CASE_BEGIN test_Render_Thread_9 */
/* CPPTEST_TEST_CASE_CONTEXT DWORD Render::Render_Thread(void *) */
void TestSuite_DeadLock_cpp_2788e23e::test_Render_Thread_9()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (unknown) */ 
    void * _arg1  = 0 ;
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = 100;
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 1L;
         ::changePositionMutex.RecursionCount  = 0;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 0ul;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::DWORD _return  = ::Render::Render_Thread(_arg1);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_UINTEGER("::DWORD _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("void * _arg1 ", ( _arg1 ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_Render_Thread_9 */

/* CPPTEST_TEST_CASE_BEGIN test_assertion_1 */
/* CPPTEST_TEST_CASE_CONTEXT void assertion(int, const char *) */
void TestSuite_DeadLock_cpp_2788e23e::test_assertion_1()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (condition) */ 
    int _condition  = cpptestLimitsGetMaxInt();
    /* Initializing argument 2 (message) */ 
    const char * _message  = "";
    /* Tested function call */
    ::assertion(_condition, _message);
    /* Post-condition check */
}
/* CPPTEST_TEST_CASE_END test_assertion_1 */

/* CPPTEST_TEST_CASE_BEGIN test_assertion_10 */
/* CPPTEST_TEST_CASE_CONTEXT void assertion(int, const char *) */
void TestSuite_DeadLock_cpp_2788e23e::test_assertion_10()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (condition) */ 
    int _condition  = -1;
    /* Initializing argument 2 (message) */ 
    const char * _message  = 0 ;
    /* Tested function call */
    ::assertion(_condition, _message);
    /* Post-condition check */
}
/* CPPTEST_TEST_CASE_END test_assertion_10 */

/* CPPTEST_TEST_CASE_BEGIN test_assertion_2 */
/* CPPTEST_TEST_CASE_CONTEXT void assertion(int, const char *) */
void TestSuite_DeadLock_cpp_2788e23e::test_assertion_2()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (condition) */ 
    int _condition  = cpptestLimitsGetMinInt();
    /* Initializing argument 2 (message) */ 
    const char * _message  = "";
    /* Tested function call */
    ::assertion(_condition, _message);
    /* Post-condition check */
}
/* CPPTEST_TEST_CASE_END test_assertion_2 */

/* CPPTEST_TEST_CASE_BEGIN test_assertion_3 */
/* CPPTEST_TEST_CASE_CONTEXT void assertion(int, const char *) */
void TestSuite_DeadLock_cpp_2788e23e::test_assertion_3()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (condition) */ 
    int _condition  = 1;
    /* Initializing argument 2 (message) */ 
    const char * _message  = "";
    /* Tested function call */
    ::assertion(_condition, _message);
    /* Post-condition check */
}
/* CPPTEST_TEST_CASE_END test_assertion_3 */

/* CPPTEST_TEST_CASE_BEGIN test_assertion_4 */
/* CPPTEST_TEST_CASE_CONTEXT void assertion(int, const char *) */
void TestSuite_DeadLock_cpp_2788e23e::test_assertion_4()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (condition) */ 
    int _condition  = cpptestLimitsGetMaxInt();
    /* Initializing argument 2 (message) */ 
    const char * _message  = "Hello world";
    /* Tested function call */
    ::assertion(_condition, _message);
    /* Post-condition check */
}
/* CPPTEST_TEST_CASE_END test_assertion_4 */

/* CPPTEST_TEST_CASE_BEGIN test_assertion_5 */
/* CPPTEST_TEST_CASE_CONTEXT void assertion(int, const char *) */
void TestSuite_DeadLock_cpp_2788e23e::test_assertion_5()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (condition) */ 
    int _condition  = -1;
    /* Initializing argument 2 (message) */ 
    const char * _message  = "Hello world";
    /* Tested function call */
    ::assertion(_condition, _message);
    /* Post-condition check */
}
/* CPPTEST_TEST_CASE_END test_assertion_5 */

/* CPPTEST_TEST_CASE_BEGIN test_assertion_6 */
/* CPPTEST_TEST_CASE_CONTEXT void assertion(int, const char *) */
void TestSuite_DeadLock_cpp_2788e23e::test_assertion_6()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (condition) */ 
    int _condition  = 0;
    /* Initializing argument 2 (message) */ 
    const char * _message  = "Hello world";
    /* Tested function call */
    ::assertion(_condition, _message);
    /* Post-condition check */
}
/* CPPTEST_TEST_CASE_END test_assertion_6 */

/* CPPTEST_TEST_CASE_BEGIN test_assertion_7 */
/* CPPTEST_TEST_CASE_CONTEXT void assertion(int, const char *) */
void TestSuite_DeadLock_cpp_2788e23e::test_assertion_7()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (condition) */ 
    int _condition  = cpptestLimitsGetMinInt();
    /* Initializing argument 2 (message) */ 
    const char * _message  = "A very long string that is supposed to contain more than 256 characters in length..................................................................................................................................................................................";
    /* Tested function call */
    ::assertion(_condition, _message);
    /* Post-condition check */
}
/* CPPTEST_TEST_CASE_END test_assertion_7 */

/* CPPTEST_TEST_CASE_BEGIN test_assertion_8 */
/* CPPTEST_TEST_CASE_CONTEXT void assertion(int, const char *) */
void TestSuite_DeadLock_cpp_2788e23e::test_assertion_8()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (condition) */ 
    int _condition  = 1;
    /* Initializing argument 2 (message) */ 
    const char * _message  = "A very long string that is supposed to contain more than 256 characters in length..................................................................................................................................................................................";
    /* Tested function call */
    ::assertion(_condition, _message);
    /* Post-condition check */
}
/* CPPTEST_TEST_CASE_END test_assertion_8 */

/* CPPTEST_TEST_CASE_BEGIN test_assertion_9 */
/* CPPTEST_TEST_CASE_CONTEXT void assertion(int, const char *) */
void TestSuite_DeadLock_cpp_2788e23e::test_assertion_9()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (condition) */ 
    int _condition  = cpptestLimitsGetMaxInt();
    /* Initializing argument 2 (message) */ 
    const char * _message  = 0 ;
    /* Tested function call */
    ::assertion(_condition, _message);
    /* Post-condition check */
}
/* CPPTEST_TEST_CASE_END test_assertion_9 */

/* CPPTEST_TEST_CASE_BEGIN test_draw_1 */
/* CPPTEST_TEST_CASE_CONTEXT void Render::draw(const Shape *) */
void TestSuite_DeadLock_cpp_2788e23e::test_draw_1()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (object) */ 
    const ::Shape * _object  = 0 ;
    /* Tested function call */
    ::Render::draw(_object);
    /* Post-condition check */
}
/* CPPTEST_TEST_CASE_END test_draw_1 */

/* CPPTEST_TEST_CASE_BEGIN test_runGameThreads2_1 */
/* CPPTEST_TEST_CASE_CONTEXT void runGameThreads2(void) */
void TestSuite_DeadLock_cpp_2788e23e::test_runGameThreads2_1()
{
    /* Pre-condition initialization */
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = cpptestLimitsGetMaxPosDouble();
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 0;
         ::changePositionMutex.RecursionCount  = 0;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 0;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::runGameThreads2();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_runGameThreads2_1 */

/* CPPTEST_TEST_CASE_BEGIN test_runGameThreads2_10 */
/* CPPTEST_TEST_CASE_CONTEXT void runGameThreads2(void) */
void TestSuite_DeadLock_cpp_2788e23e::test_runGameThreads2_10()
{
    /* Pre-condition initialization */
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = cpptestLimitsGetMaxPosDouble();
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = cpptestLimitsGetMinLong();
         ::changePositionMutex.RecursionCount  = -1L;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 0ul;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::runGameThreads2();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_runGameThreads2_10 */

/* CPPTEST_TEST_CASE_BEGIN test_runGameThreads2_2 */
/* CPPTEST_TEST_CASE_CONTEXT void runGameThreads2(void) */
void TestSuite_DeadLock_cpp_2788e23e::test_runGameThreads2_2()
{
    /* Pre-condition initialization */
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = cpptestLimitsGetMaxInt();
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = cpptestLimitsGetMaxPosDouble();
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 0;
         ::changePositionMutex.RecursionCount  = 0;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 0;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::runGameThreads2();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_runGameThreads2_2 */

/* CPPTEST_TEST_CASE_BEGIN test_runGameThreads2_3 */
/* CPPTEST_TEST_CASE_CONTEXT void runGameThreads2(void) */
void TestSuite_DeadLock_cpp_2788e23e::test_runGameThreads2_3()
{
    /* Pre-condition initialization */
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = cpptestLimitsGetMaxPosDouble();
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 1L;
         ::changePositionMutex.RecursionCount  = cpptestLimitsGetMinLong();
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 0;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::runGameThreads2();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_runGameThreads2_3 */

/* CPPTEST_TEST_CASE_BEGIN test_runGameThreads2_4 */
/* CPPTEST_TEST_CASE_CONTEXT void runGameThreads2(void) */
void TestSuite_DeadLock_cpp_2788e23e::test_runGameThreads2_4()
{
    /* Pre-condition initialization */
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = cpptestLimitsGetMaxPosDouble();
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = cpptestLimitsGetMinLong();
         ::changePositionMutex.RecursionCount  = 0L;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 0;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::runGameThreads2();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_runGameThreads2_4 */

/* CPPTEST_TEST_CASE_BEGIN test_runGameThreads2_5 */
/* CPPTEST_TEST_CASE_CONTEXT void runGameThreads2(void) */
void TestSuite_DeadLock_cpp_2788e23e::test_runGameThreads2_5()
{
    /* Pre-condition initialization */
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = cpptestLimitsGetMaxPosDouble();
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 0;
         ::changePositionMutex.RecursionCount  = cpptestLimitsGetMinLong();
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = cpptestLimitsGetMaxUnsignedLong();
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::runGameThreads2();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_runGameThreads2_5 */

/* CPPTEST_TEST_CASE_BEGIN test_runGameThreads2_6 */
/* CPPTEST_TEST_CASE_CONTEXT void runGameThreads2(void) */
void TestSuite_DeadLock_cpp_2788e23e::test_runGameThreads2_6()
{
    /* Pre-condition initialization */
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = cpptestLimitsGetMaxPosDouble();
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 1L;
         ::changePositionMutex.RecursionCount  = 1L;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = cpptestLimitsGetMaxUnsignedLong();
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::runGameThreads2();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_runGameThreads2_6 */

/* CPPTEST_TEST_CASE_BEGIN test_runGameThreads2_7 */
/* CPPTEST_TEST_CASE_CONTEXT void runGameThreads2(void) */
void TestSuite_DeadLock_cpp_2788e23e::test_runGameThreads2_7()
{
    /* Pre-condition initialization */
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = cpptestLimitsGetMaxPosDouble();
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = cpptestLimitsGetMinLong();
         ::changePositionMutex.RecursionCount  = cpptestLimitsGetMaxLong();
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 1ul;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::runGameThreads2();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_runGameThreads2_7 */

/* CPPTEST_TEST_CASE_BEGIN test_runGameThreads2_8 */
/* CPPTEST_TEST_CASE_CONTEXT void runGameThreads2(void) */
void TestSuite_DeadLock_cpp_2788e23e::test_runGameThreads2_8()
{
    /* Pre-condition initialization */
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = cpptestLimitsGetMaxPosDouble();
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 0;
         ::changePositionMutex.RecursionCount  = 1L;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 1ul;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::runGameThreads2();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_runGameThreads2_8 */

/* CPPTEST_TEST_CASE_BEGIN test_runGameThreads2_9 */
/* CPPTEST_TEST_CASE_CONTEXT void runGameThreads2(void) */
void TestSuite_DeadLock_cpp_2788e23e::test_runGameThreads2_9()
{
    /* Pre-condition initialization */
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = cpptestLimitsGetMaxPosDouble();
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 1L;
         ::changePositionMutex.RecursionCount  = 0;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 0ul;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::runGameThreads2();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_runGameThreads2_9 */

/* CPPTEST_TEST_CASE_BEGIN test_runGameThreads_1 */
/* CPPTEST_TEST_CASE_CONTEXT void runGameThreads(void) */
void TestSuite_DeadLock_cpp_2788e23e::test_runGameThreads_1()
{
    /* Pre-condition initialization */
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = cpptestLimitsGetMaxPosDouble();
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 0;
         ::changePositionMutex.RecursionCount  = 0;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 0;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::runGameThreads();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_runGameThreads_1 */

/* CPPTEST_TEST_CASE_BEGIN test_runGameThreads_10 */
/* CPPTEST_TEST_CASE_CONTEXT void runGameThreads(void) */
void TestSuite_DeadLock_cpp_2788e23e::test_runGameThreads_10()
{
    /* Pre-condition initialization */
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = cpptestLimitsGetMaxPosDouble();
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = cpptestLimitsGetMinLong();
         ::changePositionMutex.RecursionCount  = -1L;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 0ul;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::runGameThreads();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_runGameThreads_10 */

/* CPPTEST_TEST_CASE_BEGIN test_runGameThreads_2 */
/* CPPTEST_TEST_CASE_CONTEXT void runGameThreads(void) */
void TestSuite_DeadLock_cpp_2788e23e::test_runGameThreads_2()
{
    /* Pre-condition initialization */
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = cpptestLimitsGetMaxInt();
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = cpptestLimitsGetMaxPosDouble();
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 0;
         ::changePositionMutex.RecursionCount  = 0;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 0;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::runGameThreads();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_runGameThreads_2 */

/* CPPTEST_TEST_CASE_BEGIN test_runGameThreads_3 */
/* CPPTEST_TEST_CASE_CONTEXT void runGameThreads(void) */
void TestSuite_DeadLock_cpp_2788e23e::test_runGameThreads_3()
{
    /* Pre-condition initialization */
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = cpptestLimitsGetMaxPosDouble();
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 1L;
         ::changePositionMutex.RecursionCount  = cpptestLimitsGetMinLong();
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 0;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::runGameThreads();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_runGameThreads_3 */

/* CPPTEST_TEST_CASE_BEGIN test_runGameThreads_4 */
/* CPPTEST_TEST_CASE_CONTEXT void runGameThreads(void) */
void TestSuite_DeadLock_cpp_2788e23e::test_runGameThreads_4()
{
    /* Pre-condition initialization */
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = cpptestLimitsGetMaxPosDouble();
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = cpptestLimitsGetMinLong();
         ::changePositionMutex.RecursionCount  = 0L;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 0;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::runGameThreads();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_runGameThreads_4 */

/* CPPTEST_TEST_CASE_BEGIN test_runGameThreads_5 */
/* CPPTEST_TEST_CASE_CONTEXT void runGameThreads(void) */
void TestSuite_DeadLock_cpp_2788e23e::test_runGameThreads_5()
{
    /* Pre-condition initialization */
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = cpptestLimitsGetMaxPosDouble();
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 0;
         ::changePositionMutex.RecursionCount  = cpptestLimitsGetMinLong();
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = cpptestLimitsGetMaxUnsignedLong();
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::runGameThreads();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_runGameThreads_5 */

/* CPPTEST_TEST_CASE_BEGIN test_runGameThreads_6 */
/* CPPTEST_TEST_CASE_CONTEXT void runGameThreads(void) */
void TestSuite_DeadLock_cpp_2788e23e::test_runGameThreads_6()
{
    /* Pre-condition initialization */
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = cpptestLimitsGetMaxPosDouble();
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 1L;
         ::changePositionMutex.RecursionCount  = 1L;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = cpptestLimitsGetMaxUnsignedLong();
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::runGameThreads();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_runGameThreads_6 */

/* CPPTEST_TEST_CASE_BEGIN test_runGameThreads_7 */
/* CPPTEST_TEST_CASE_CONTEXT void runGameThreads(void) */
void TestSuite_DeadLock_cpp_2788e23e::test_runGameThreads_7()
{
    /* Pre-condition initialization */
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = cpptestLimitsGetMaxPosDouble();
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = cpptestLimitsGetMinLong();
         ::changePositionMutex.RecursionCount  = cpptestLimitsGetMaxLong();
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 1ul;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::runGameThreads();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_runGameThreads_7 */

/* CPPTEST_TEST_CASE_BEGIN test_runGameThreads_8 */
/* CPPTEST_TEST_CASE_CONTEXT void runGameThreads(void) */
void TestSuite_DeadLock_cpp_2788e23e::test_runGameThreads_8()
{
    /* Pre-condition initialization */
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = cpptestLimitsGetMaxPosDouble();
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 0;
         ::changePositionMutex.RecursionCount  = 1L;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 1ul;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::runGameThreads();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_runGameThreads_8 */

/* CPPTEST_TEST_CASE_BEGIN test_runGameThreads_9 */
/* CPPTEST_TEST_CASE_CONTEXT void runGameThreads(void) */
void TestSuite_DeadLock_cpp_2788e23e::test_runGameThreads_9()
{
    /* Pre-condition initialization */
    /* Initializing global variable ::Physics::velocityArray */ 
    {
         ::Physics::velocityArray[0]  = 0 ;
    }
    /* Initializing global variable ::participantsCount */ 
    {
         ::participantsCount  = 0;
    }
    /* Initializing global variable ::ring */ 
    {
         ::ring._radius  = cpptestLimitsGetMaxPosDouble();
    }
    /* Initializing global variable ::exitGame */ 
    {
         ::exitGame  = 0;
    }
    /* Initializing global variable ::changePositionMutex */ 
    {
         ::changePositionMutex.DebugInfo  = 0 ;
         ::changePositionMutex.LockCount  = 1L;
         ::changePositionMutex.RecursionCount  = 0;
         ::changePositionMutex.OwningThread  = 0 ;
         ::changePositionMutex.LockSemaphore  = 0 ;
         ::changePositionMutex.SpinCount  = 0ul;
    }
    /* Initializing global variable ::participants */ 
    {
         ::participants[0]  = 0 ;
    }
    /* Initializing global variable ::Render::currentCameraVelocity */ 
    {
         ::Render::currentCameraVelocity  = 0 ;
    }
    /* Tested function call */
    ::runGameThreads();
    /* Post-condition check */
    CPPTEST_POST_CONDITION_PTR("Point * ::Physics::velocityArray[0] ", ( ::Physics::velocityArray[0] ));
    CPPTEST_POST_CONDITION_INTEGER("int ::participantsCount", ( ::participantsCount ));
    CPPTEST_POST_CONDITION_FLOAT("double ::ring._radius", ( ::ring._radius ));
    CPPTEST_POST_CONDITION_INTEGER("int ::exitGame", ( ::exitGame ));
    CPPTEST_POST_CONDITION_PTR("::PRTL_CRITICAL_SECTION_DEBUG ::changePositionMutex.DebugInfo", ( ::changePositionMutex.DebugInfo ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.LockCount", ( ::changePositionMutex.LockCount ));
    CPPTEST_POST_CONDITION_INTEGER("::LONG ::changePositionMutex.RecursionCount", ( ::changePositionMutex.RecursionCount ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.OwningThread", ( ::changePositionMutex.OwningThread ));
    CPPTEST_POST_CONDITION_PTR("::HANDLE ::changePositionMutex.LockSemaphore", ( ::changePositionMutex.LockSemaphore ));
    CPPTEST_POST_CONDITION_UINTEGER("::ULONG_PTR ::changePositionMutex.SpinCount", ( ::changePositionMutex.SpinCount ));
    CPPTEST_POST_CONDITION_PTR("Shape * ::participants[0] ", ( ::participants[0] ));
    CPPTEST_POST_CONDITION_PTR("Point * ::Render::currentCameraVelocity ", ( ::Render::currentCameraVelocity ));
}
/* CPPTEST_TEST_CASE_END test_runGameThreads_9 */
