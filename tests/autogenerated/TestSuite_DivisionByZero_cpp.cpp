#include "cpptest.h"

/* CPPTEST_TEST_SUITE_CODE_BEGIN AdditionalIncludes */
/* CPPTEST_TEST_SUITE_CODE_END AdditionalIncludes */

CPPTEST_CONTEXT("/FlowAnalysisCpp/DivisionByZero.cpp");
CPPTEST_TEST_SUITE_INCLUDED_TO("/FlowAnalysisCpp/DivisionByZero.cpp");

class TestSuite_DivisionByZero_cpp_e7ecfacc : public CppTest_TestSuite
{
    public:
        CPPTEST_TEST_SUITE(TestSuite_DivisionByZero_cpp_e7ecfacc);
        CPPTEST_TEST_SUITE_SETUP(testSuiteSetUp);
        CPPTEST_TEST(test_getShapeRatio_1);
        CPPTEST_TEST_SUITE_TEARDOWN(testSuiteTearDown);
        CPPTEST_TEST_SUITE_END();
        
        static void testSuiteSetUp();
        static void testSuiteTearDown();

        void setUp();
        void tearDown();

        void test_getShapeRatio_1();
};

CPPTEST_TEST_SUITE_REGISTRATION(TestSuite_DivisionByZero_cpp_e7ecfacc);

void TestSuite_DivisionByZero_cpp_e7ecfacc::testSuiteSetUp()
{
/* CPPTEST_TEST_SUITE_CODE_BEGIN TestSuiteSetUp */
/* CPPTEST_TEST_SUITE_CODE_END TestSuiteSetUp */
}

void TestSuite_DivisionByZero_cpp_e7ecfacc::testSuiteTearDown()
{
/* CPPTEST_TEST_SUITE_CODE_BEGIN TestSuiteTearDown */
/* CPPTEST_TEST_SUITE_CODE_END TestSuiteTearDown */
}

void TestSuite_DivisionByZero_cpp_e7ecfacc::setUp()
{
/* CPPTEST_TEST_SUITE_CODE_BEGIN TestCaseSetUp */
/* CPPTEST_TEST_SUITE_CODE_END TestCaseSetUp */
}

void TestSuite_DivisionByZero_cpp_e7ecfacc::tearDown()
{
/* CPPTEST_TEST_SUITE_CODE_BEGIN TestCaseTearDown */
/* CPPTEST_TEST_SUITE_CODE_END TestCaseTearDown */
}

/* CPPTEST_TEST_CASE_BEGIN test_getShapeRatio_1 */
/* CPPTEST_TEST_CASE_CONTEXT double getShapeRatio(Shape *, Shape *) */
void TestSuite_DivisionByZero_cpp_e7ecfacc::test_getShapeRatio_1()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (shape1) */ 
    ::Shape * _shape1  = 0 ;
    /* Initializing argument 2 (shape2) */ 
    ::Shape * _shape2  = 0 ;
    /* Tested function call */
    double _return  = ::getShapeRatio(_shape1, _shape2);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_FLOAT("double _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("Shape * _shape1 ", ( _shape1 ));
    CPPTEST_POST_CONDITION_PTR("Shape * _shape2 ", ( _shape2 ));
}
/* CPPTEST_TEST_CASE_END test_getShapeRatio_1 */
