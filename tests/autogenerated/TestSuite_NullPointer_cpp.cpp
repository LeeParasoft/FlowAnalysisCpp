#include "cpptest.h"

/* CPPTEST_TEST_SUITE_CODE_BEGIN AdditionalIncludes */
/* CPPTEST_TEST_SUITE_CODE_END AdditionalIncludes */

CPPTEST_CONTEXT("/FlowAnalysisCpp/NullPointer.cpp");
CPPTEST_TEST_SUITE_INCLUDED_TO("/FlowAnalysisCpp/NullPointer.cpp");

class TestSuite_NullPointer_cpp_d41cc372 : public CppTest_TestSuite
{
    public:
        CPPTEST_TEST_SUITE(TestSuite_NullPointer_cpp_d41cc372);
        CPPTEST_TEST_SUITE_SETUP(testSuiteSetUp);
        CPPTEST_TEST(test_main_1);
        CPPTEST_TEST(test_main_10);
        CPPTEST_TEST(test_main_2);
        CPPTEST_TEST(test_main_3);
        CPPTEST_TEST(test_main_4);
        CPPTEST_TEST(test_main_5);
        CPPTEST_TEST(test_main_6);
        CPPTEST_TEST(test_main_7);
        CPPTEST_TEST(test_main_8);
        CPPTEST_TEST(test_main_9);
        CPPTEST_TEST_SUITE_TEARDOWN(testSuiteTearDown);
        CPPTEST_TEST_SUITE_END();
        
        static void testSuiteSetUp();
        static void testSuiteTearDown();

        void setUp();
        void tearDown();

        void test_main_1();
        void test_main_10();
        void test_main_2();
        void test_main_3();
        void test_main_4();
        void test_main_5();
        void test_main_6();
        void test_main_7();
        void test_main_8();
        void test_main_9();
};

CPPTEST_TEST_SUITE_REGISTRATION(TestSuite_NullPointer_cpp_d41cc372);

void TestSuite_NullPointer_cpp_d41cc372::testSuiteSetUp()
{
/* CPPTEST_TEST_SUITE_CODE_BEGIN TestSuiteSetUp */
/* CPPTEST_TEST_SUITE_CODE_END TestSuiteSetUp */
}

void TestSuite_NullPointer_cpp_d41cc372::testSuiteTearDown()
{
/* CPPTEST_TEST_SUITE_CODE_BEGIN TestSuiteTearDown */
/* CPPTEST_TEST_SUITE_CODE_END TestSuiteTearDown */
}

void TestSuite_NullPointer_cpp_d41cc372::setUp()
{
/* CPPTEST_TEST_SUITE_CODE_BEGIN TestCaseSetUp */
/* CPPTEST_TEST_SUITE_CODE_END TestCaseSetUp */
}

void TestSuite_NullPointer_cpp_d41cc372::tearDown()
{
/* CPPTEST_TEST_SUITE_CODE_BEGIN TestCaseTearDown */
/* CPPTEST_TEST_SUITE_CODE_END TestCaseTearDown */
}

/* CPPTEST_TEST_CASE_BEGIN test_main_1 */
/* CPPTEST_TEST_CASE_CONTEXT int main(int, char **) */
void TestSuite_NullPointer_cpp_d41cc372::test_main_1()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (argc) */ 
    int _argc  = 2;
    /* Initializing argument 2 (argv) */ 
    char ** _argv  = 0 ;
    /* Tested function call */
    int _return  = ::main(_argc, _argv);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("char ** _argv ", ( _argv ));
}
/* CPPTEST_TEST_CASE_END test_main_1 */

/* CPPTEST_TEST_CASE_BEGIN test_main_10 */
/* CPPTEST_TEST_CASE_CONTEXT int main(int, char **) */
void TestSuite_NullPointer_cpp_d41cc372::test_main_10()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (argc) */ 
    int _argc  = 2;
    /* Initializing argument 2 (argv) */ 
    char * _argv_0  = 0 ;
    char ** _argv  = & _argv_0;
    /* Tested function call */
    int _return  = ::main(_argc, _argv);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("char ** _argv ", ( _argv ));
}
/* CPPTEST_TEST_CASE_END test_main_10 */

/* CPPTEST_TEST_CASE_BEGIN test_main_2 */
/* CPPTEST_TEST_CASE_CONTEXT int main(int, char **) */
void TestSuite_NullPointer_cpp_d41cc372::test_main_2()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (argc) */ 
    int _argc  = 1;
    /* Initializing argument 2 (argv) */ 
    char ** _argv  = 0 ;
    /* Tested function call */
    int _return  = ::main(_argc, _argv);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("char ** _argv ", ( _argv ));
}
/* CPPTEST_TEST_CASE_END test_main_2 */

/* CPPTEST_TEST_CASE_BEGIN test_main_3 */
/* CPPTEST_TEST_CASE_CONTEXT int main(int, char **) */
void TestSuite_NullPointer_cpp_d41cc372::test_main_3()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (argc) */ 
    int _argc  = cpptestLimitsGetMinInt();
    /* Initializing argument 2 (argv) */ 
    char ** _argv  = 0 ;
    /* Tested function call */
    int _return  = ::main(_argc, _argv);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("char ** _argv ", ( _argv ));
}
/* CPPTEST_TEST_CASE_END test_main_3 */

/* CPPTEST_TEST_CASE_BEGIN test_main_4 */
/* CPPTEST_TEST_CASE_CONTEXT int main(int, char **) */
void TestSuite_NullPointer_cpp_d41cc372::test_main_4()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (argc) */ 
    int _argc  = 0;
    /* Initializing argument 2 (argv) */ 
    char _argv_0_1 [1];
    char * _argv_0  = (char * )cpptestMemset((void*)&_argv_0_1, 0, (1 * sizeof(char)));
    char ** _argv  = & _argv_0;
    /* Tested function call */
    int _return  = ::main(_argc, _argv);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("char ** _argv ", ( _argv ));
}
/* CPPTEST_TEST_CASE_END test_main_4 */

/* CPPTEST_TEST_CASE_BEGIN test_main_5 */
/* CPPTEST_TEST_CASE_CONTEXT int main(int, char **) */
void TestSuite_NullPointer_cpp_d41cc372::test_main_5()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (argc) */ 
    int _argc  = -1;
    /* Initializing argument 2 (argv) */ 
    char _argv_0_1 [1];
    char * _argv_0  = (char * )cpptestMemset((void*)&_argv_0_1, 0, (1 * sizeof(char)));
    char ** _argv  = & _argv_0;
    /* Tested function call */
    int _return  = ::main(_argc, _argv);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("char ** _argv ", ( _argv ));
}
/* CPPTEST_TEST_CASE_END test_main_5 */

/* CPPTEST_TEST_CASE_BEGIN test_main_6 */
/* CPPTEST_TEST_CASE_CONTEXT int main(int, char **) */
void TestSuite_NullPointer_cpp_d41cc372::test_main_6()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (argc) */ 
    int _argc  = 4;
    /* Initializing argument 2 (argv) */ 
    char _argv_0_1 [16];
    char * _argv_0  = (char * )cpptestMemset((void*)&_argv_0_1, 0, (16 * sizeof(char)));
    char ** _argv  = & _argv_0;
    /* Tested function call */
    int _return  = ::main(_argc, _argv);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("char ** _argv ", ( _argv ));
}
/* CPPTEST_TEST_CASE_END test_main_6 */

/* CPPTEST_TEST_CASE_BEGIN test_main_7 */
/* CPPTEST_TEST_CASE_CONTEXT int main(int, char **) */
void TestSuite_NullPointer_cpp_d41cc372::test_main_7()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (argc) */ 
    int _argc  = 1;
    /* Initializing argument 2 (argv) */ 
    char _argv_0_1 [16];
    char * _argv_0  = (char * )cpptestMemset((void*)&_argv_0_1, 0, (16 * sizeof(char)));
    char ** _argv  = & _argv_0;
    /* Tested function call */
    int _return  = ::main(_argc, _argv);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("char ** _argv ", ( _argv ));
}
/* CPPTEST_TEST_CASE_END test_main_7 */

/* CPPTEST_TEST_CASE_BEGIN test_main_8 */
/* CPPTEST_TEST_CASE_CONTEXT int main(int, char **) */
void TestSuite_NullPointer_cpp_d41cc372::test_main_8()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (argc) */ 
    int _argc  = 3;
    /* Initializing argument 2 (argv) */ 
    char _argv_0_1 [256];
    char * _argv_0  = (char * )cpptestMemset((void*)&_argv_0_1, 0, (256 * sizeof(char)));
    char ** _argv  = & _argv_0;
    /* Tested function call */
    int _return  = ::main(_argc, _argv);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("char ** _argv ", ( _argv ));
}
/* CPPTEST_TEST_CASE_END test_main_8 */

/* CPPTEST_TEST_CASE_BEGIN test_main_9 */
/* CPPTEST_TEST_CASE_CONTEXT int main(int, char **) */
void TestSuite_NullPointer_cpp_d41cc372::test_main_9()
{
    /* Pre-condition initialization */
    /* Initializing argument 1 (argc) */ 
    int _argc  = 0;
    /* Initializing argument 2 (argv) */ 
    char _argv_0_1 [256];
    char * _argv_0  = (char * )cpptestMemset((void*)&_argv_0_1, 0, (256 * sizeof(char)));
    char ** _argv  = & _argv_0;
    /* Tested function call */
    int _return  = ::main(_argc, _argv);
    /* Post-condition check */
    CPPTEST_POST_CONDITION_INTEGER("int _return", ( _return ));
    CPPTEST_POST_CONDITION_PTR("char ** _argv ", ( _argv ));
}
/* CPPTEST_TEST_CASE_END test_main_9 */
